# WT32-ETH01_PCB_3D-model

A WT32-ETH01 3D model.

# Preview

![preview-3d](https://codeberg.org/BluePixel4k/WT32-ETH01_PCB_3D-model/raw/branch/main/preview-3d.jpg)

# Datasheet
https://files.seeedstudio.com/products/102991455/WT32-ETH01_datasheet_V1.1-%20en.pdf

# License

CC0 1.0 Universal (CC0 1.0)
Public Domain Dedication